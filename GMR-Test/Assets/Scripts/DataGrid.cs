﻿using System.Collections;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

namespace JsonChallenge
{
    public class DataGrid : SyncBehaviour, ISync
    {
        public GameObject rowPrefab;
        public GameObject cellPrefab;

        JSONNode _rows;
        int _columnsCount;

        #region Interface Methods
        public void Sync()
        {
            _rows = DataLoader.data["Data"];
            _columnsCount = DataLoader.data["ColumnHeaders"].Count;
            ClearChildren();
            Transform row;
            Transform child;//cell transform
            Text cell;
            for (int i = 0; i < _rows.Count; i++)
            {
                row = transform.childCount > i ? transform.GetChild(i) : null;
                if (!row) row = Instantiate(rowPrefab, transform).transform;
                row.gameObject.SetActive(true);
                for (int j = 0; j < _columnsCount; j++)
                {
                    child = row.childCount > j ? row.GetChild(j) : null;
                    if (!child) child = Instantiate(cellPrefab, row).transform;
                    cell = child.GetComponent<Text>();
                    cell.text = _rows[i][j];
                    cell.gameObject.SetActive(true);
                }
            }
        }
        #endregion

        IEnumerator Start()
        {
            yield return new WaitUntil(() => DataLoader.data != null);
            Sync();
        }

        #region Methods
        /// <summary>
        /// Disable extra children (rows and cells/columns) if there are less after the sync.
        /// </summary>
        void ClearChildren()
        {
            int rowsCount = transform.childCount;
            Transform row;
            int colsCount;
            for (int i=0; i<rowsCount; i++)
            {
                row = transform.GetChild(i);
                colsCount = row.childCount;
                if (_columnsCount == 0) _columnsCount = _rows[i].Count;
                for (int j=_columnsCount; j<colsCount; j++)
                {
                    row.GetChild(j).gameObject.SetActive(false);
                }
                if (i >= _rows.Count - 1) row.gameObject.SetActive(false);
            }
        }
        #endregion
    }
}
