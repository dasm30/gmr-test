﻿using UnityEngine;
using System.IO;
using SimpleJSON;

namespace JsonChallenge {
    public class DataLoader : MonoBehaviour
    {
        public string fileName = "JsonChallenge";
        public bool enableFileNameChangeListener = true;
        public GameObject[] syncs;

        public static JSONNode data;

        string newFileName;


        void Awake()
        {
            LoadJsonFromStreamingAssets();
            ValidateSyncBehaviours();
        }
        void FixedUpdate()
        {
            if (!enableFileNameChangeListener) return;
            if (!fileName.Equals(newFileName))
            {
                if (newFileName != null) ReSync();
                newFileName = fileName;
            }
        }

        #region Public Methods
        public void ReSync()
        {
            Debug.Log("ReSync!");
            LoadJsonFromStreamingAssets();
            if (data == null) return;
            GameObject syncObj;
            FixPivot fixPivot;
            ISync sync;
            for (int i = 0; i < syncs.Length; i++)
            {
                syncObj = syncs[i];
                sync = (ISync)syncObj.GetComponent<SyncBehaviour>();
                sync.Sync();
                fixPivot = syncObj.GetComponent<FixPivot>();
                if (fixPivot) StartCoroutine(fixPivot.Fix());
            }
        }
        #endregion

        #region Private Methods
        void ValidateSyncBehaviours()
        {
            if (syncs.Length == 0)
            {
                syncs = GameObject.FindGameObjectsWithTag("Sync");
            }
        }
        void LoadJsonFromStreamingAssets()
        {
            string filePath = Path.Combine(Application.streamingAssetsPath, fileName + ".json");

            if (File.Exists(filePath))
            {
                string json = File.ReadAllText(filePath);
                data = JSON.Parse(json);
            }
            else
            {
                Debug.LogError("Cannot load json data!");
            }
        }
        #endregion
    }
}
