﻿using System.Collections;
using UnityEngine;

namespace JsonChallenge
{
    public class FixPivot : MonoBehaviour
    {
        RectTransform _rt;
        Rect _rect;
        Rect _parentRect;

        IEnumerator Start()
        {
            return Fix();
        }

        public IEnumerator Fix()
        {
            yield return new WaitForEndOfFrame();
            _rt = GetComponent<RectTransform>();
            _rect = GetComponent<RectTransform>().rect;
            _parentRect = transform.parent.GetComponent<RectTransform>().rect;
            if (_rect.width < _parentRect.width) //center
            {
                _rt.pivot = new Vector2(0.5f, _rt.pivot.y);
            }
            else _rt.pivot = new Vector2(0f, _rt.pivot.y);
        }
    }
}
