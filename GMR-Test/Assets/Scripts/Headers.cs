﻿using System.Collections;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

namespace JsonChallenge
{
    public class Headers : SyncBehaviour, ISync
    {
        public GameObject headerPrefab;

        JSONNode _headers;

        #region Interface Methods
        public void Sync()
        {
            _headers = DataLoader.data["ColumnHeaders"];
            ClearChildren();
            Transform col;
            Text header;
            for (int i = 0; i < _headers.Count; i++)
            {
                col = transform.childCount > i ? transform.GetChild(i) : null;
                if (!col) col = Instantiate(headerPrefab, transform).transform;
                header = col.GetComponent<Text>();
                header.text = _headers[i];
                header.gameObject.SetActive(true);
            }
        }
        #endregion

        IEnumerator Start()
        {
            yield return new WaitUntil(() => DataLoader.data != null);
            Sync();
        }

        #region Methods
        /// <summary>
        /// Disable extra children (headers/columns) if there are less after the sync.
        /// </summary>
        void ClearChildren()
        {
            int colsCount = transform.childCount;
            for (int i = 0; i < colsCount; i++)
            {
                if (i >= _headers.Count - 1) transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        #endregion
    }
}