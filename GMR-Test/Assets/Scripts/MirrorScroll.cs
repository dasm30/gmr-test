﻿using UnityEngine;
using UnityEngine.UI;

namespace JsonChallenge
{
    public class MirrorScroll : MonoBehaviour
    {
        public ScrollRect scrollView;
        public ScrollRect mirrorView;


        void Start()
        {
            if (scrollView) scrollView = GetComponentInChildren<ScrollRect>();
            if (mirrorView == null)
            {
                Debug.LogWarning("No Mirror Scroll View has been specified, disabling script");
                enabled = false;
            }
            scrollView.onValueChanged.AddListener(DoMirrorScroll);
        }

        void DoMirrorScroll(Vector2 newPos)
        {
            mirrorView.normalizedPosition = newPos;
        }
    }
}
