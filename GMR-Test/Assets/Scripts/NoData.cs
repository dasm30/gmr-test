﻿using System.Collections;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

namespace JsonChallenge
{
    public class NoData : MonoBehaviour, ISync
    {
        public GameObject cellPrefab;

        JSONNode _cells;
        JSONNode _data;

        #region Interface Methods
        public void Sync()
        {
            if (_data != null) return;
            ClearChildren();
            Transform col;
            Text cell;
            for (int i = 0; i < _cells.Count; i++)
            {
                col = transform.childCount > i ? transform.GetChild(i) : null;
                if (!col) col = Instantiate(cellPrefab, transform).transform;
                cell = col.GetComponent<Text>();
                cell.text = "-";
                cell.gameObject.SetActive(true);
            }
        }
        #endregion

        IEnumerator Start()
        {
            yield return new WaitUntil(() => DataLoader.data != null);
            _cells = DataLoader.data["ColumnHeaders"];
            _data = DataLoader.data["Data"];
            Sync();
        }

        #region Methods
        /// <summary>
        /// Disable extra children (cells/columns) if there are less after the sync.
        /// </summary>
        void ClearChildren()
        {
            int colsCount = transform.childCount;
            for (int i = 0; i < colsCount; i++)
            {
                if (i >= _cells.Count - 1) transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        #endregion
    }
}
