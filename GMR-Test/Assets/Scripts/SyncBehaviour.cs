﻿using UnityEngine;

namespace JsonChallenge
{
    public abstract class SyncBehaviour : MonoBehaviour
    {
        protected virtual void Awake()
        {
            gameObject.tag = "Sync";
        }
    }
}
