﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace JsonChallenge
{
    public class Title : SyncBehaviour, ISync
    {
        public Text text;

        #region Interface Methods
        public void Sync()
        {
            text.text = DataLoader.data["Title"];
            text.gameObject.SetActive(!string.IsNullOrEmpty(text.text));
        }
        #endregion

        IEnumerator Start()
        {
            if (!text) text = GetComponent<Text>();
            yield return new WaitUntil(() => DataLoader.data != null);
            Sync();
        }
    }
}
