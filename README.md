# gmr-test

1. Decided to use SimpleJSON cause of Unity JSON Utils limitations
2. Made code as generic as possible with specific namespace.
3. Decided to Create ISync interface and SyncBehaviour to have an abstracted and independent code structure and be able to properly sync data when json gets updated.
4. Left some default inactive objects to avoid unnecessary costly instantiations and destructions, no objects are destroyed, thought about adding a script to destroy inactive objects after they are unused for long period of time to release memory, but didn't implement it, just wanted to mention it.
5. I know some Packages can be removed to reduce build size, but didn't take the time to do it, just wanted to mention it.
6. Added multiple json files for easier testing various cases in the Editor.
7. Implemented validations, compositing, and other good coding practices.

I hope you enjoy the results!